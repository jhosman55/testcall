#!/bin/bash 

echo "En el siguiente paso debe ingresar la clave del equipo"
echo ""
sudo apt update && sudo apt install -y speedtest-cli python
clear
echo ""
echo "Nombre del equipo" $HOSTNAME
echo ""
ip a | grep inet
echo ""
ping -c 5 google.com.co
echo ""
echo "Eliminando caché Firefox"
rm -rf ~/.cache/mozilla/firefox/*
rm ~/.mozilla/firefox/*release/*.sqlite
echo ""
echo "Eliminando caché Chrome"
rm ~/.config/google-chrome/Default/
rm ~/.cache/google-chrome/default
echo ""
echo "Test de velocidad"
speedtest-cli
